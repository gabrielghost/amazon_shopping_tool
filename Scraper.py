import requests
from bs4 import BeautifulSoup
import time
from selenium import webdriver


def check_price():

    URL = 'https://www.amazon.co.uk/Apple-iPhone-6S-SIM-Free-Smartphone-Space-Grey/dp/B01ALKM6A0/ref=pd_sbs_107_2/259-5261286-3355433?_encoding=UTF8&pd_rd_i=B01ALKM6A0&pd_rd_r=3b392702-b770-47f0-b989-838d7f6b7431&pd_rd_w=Oc6uj&pd_rd_wg=u6grP&pf_rd_p=2b420a2f-6593-478e-8b5f-cb43865ff16f&pf_rd_r=Q4RTH55N3181AMM002PX&psc=1&refRID=Q4RTH55N3181AMM002PX'

    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36'}

    page = requests.get(URL, headers=headers)

    soup = BeautifulSoup(page.content, 'html.parser')

    title = soup.find(id='priceblock_ourprice').get_text()

    price = float(title[1:4])  # price of iphone 6s 16GB

    print(price)
    if price < 155: #original price 155.0
        print('Hi, your product price on amazon has gone down to {price}. Check it out!')
        add_to_cart()


def add_to_cart():
    driver = webdriver.Chrome('C:/Users/C/Downloads/chromedriver_win32/chromedriver.exe')  # Optional argument, if not specified will search path.
    driver.get('https://www.amazon.co.uk/Apple-iPhone-6S-SIM-Free-Smartphone-Space-Grey/dp/B01ALKM6A0/ref=pd_sbs_107_2/259-5261286-3355433?_encoding=UTF8&pd_rd_i=B01ALKM6A0&pd_rd_r=3b392702-b770-47f0-b989-838d7f6b7431&pd_rd_w=Oc6uj&pd_rd_wg=u6grP&pf_rd_p=2b420a2f-6593-478e-8b5f-cb43865ff16f&pf_rd_r=Q4RTH55N3181AMM002PX&psc=1&refRID=Q4RTH55N3181AMM002PX')
    time.sleep(5) # Let the user actually see something!
    add_to_cart = driver.find_element_by_name('submit.add-to-cart')
    add_to_cart.submit()
    time.sleep(10) # Let the user actually see something!
    cookies = driver.get_cookies()
    print(cookies)

while True:
    check_price()
    time.sleep(1800)
